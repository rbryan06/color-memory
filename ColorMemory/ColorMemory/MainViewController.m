//
//  MainViewController.m
//  ColorMemory
//
//  Created by Raymond Brion on 19/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "MainViewController.h"
#import "Custom2DArray.h"
#import "HighScoreViewController.h"

NSString* kToHighScoreScreenSegueID = @"ToHighScoreScreen";

@interface MainViewController () <UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel* scoreLabel;
@property (nonatomic, weak) IBOutlet UILabel* pairsToGoLabel;

@property (nonatomic, assign) NSInteger score;
@property (nonatomic, assign) NSInteger pairsToGo;

@property (nonatomic, strong) Custom2DArray* gameBoard;

@property (nonatomic, weak) UIButton* currentlySelectedButton;
@property (nonatomic, assign) BOOL isOnDelay;

@property (nonatomic, strong) IBOutletCollection(UIButton)
                                NSArray* firstRowButtons;
@property (nonatomic, strong) IBOutletCollection(UIButton)
                              NSArray* secondRowButtons;
@property (nonatomic, strong) IBOutletCollection(UIButton)
                              NSArray* thirdRowButtons;
@property (nonatomic, strong) IBOutletCollection(UIButton)
                                NSArray* fourthRowButtons;

@property (nonatomic, strong) HighScore* playerScore;

@end

@implementation MainViewController

@synthesize score = m_score;
@synthesize pairsToGo = m_pairsToGo;

#pragma mark - View lifecycle

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  /* Perform game setup */
  
  self.isOnDelay = NO;
  
  [self setupPairsToGo];
  [self setupScore];
  [self setupGameboard];
}

#pragma mark - Custom getter and setter methods

- (void) setScore: (NSInteger) score
{
  if (m_score != score)
  {
    m_score = score;
    self.scoreLabel.text = [NSString stringWithFormat: @"%ld", (long) score];
  }
}

- (NSInteger) score
{
  return m_score;
}

- (void) setPairsToGo: (NSInteger) pairsToGo
{
  if (m_pairsToGo != pairsToGo)
  {
    m_pairsToGo = pairsToGo;
    self.pairsToGoLabel.text = [NSString stringWithFormat: @"%ld",
                                (long)pairsToGo];
  }
}

- (NSInteger) pairsToGo
{
  return m_pairsToGo;
}

#pragma mark - Setup methods

- (void) setupPairsToGo
{
  self.pairsToGo = 8;
}

- (void) setupScore
{
  self.score = 0;
}

- (void) setupGameboard
{
  self.gameBoard = [[Custom2DArray alloc] initWithHeight: 4
                                                andWidth: 4];
  for (NSInteger i = 1; i <= 8; i++)
  {
    [self assignButtonsValue: i];
  }
}

#pragma mark - Invalidate methods

- (void) invalidateGameboard
{
  self.gameBoard = nil;
  
  if (self.currentlySelectedButton != nil)
  {
    [self.currentlySelectedButton setSelected: NO];
  }
  
  /* Display all buttons */
  
  for (UIButton* button in self.firstRowButtons)
  {
    [button setHidden: NO];
    [button setSelected: NO];
  }
  
  for (UIButton* button in self.secondRowButtons)
  {
    [button setHidden: NO];
    [button setSelected: NO];
  }
  
  for (UIButton* button in self.thirdRowButtons)
  {
    [button setHidden: NO];
    [button setSelected: NO];
  }
  
  for (UIButton* button in self.fourthRowButtons)
  {
    [button setHidden: NO];
    [button setSelected: NO];
  }
  
  self.currentlySelectedButton = nil;
}

#pragma mark - IBAction methods

- (IBAction) unwindToMainViewController: (UIStoryboardSegue*) segue
{
  /* Setup the game all over again */
  
  [self invalidateGameboard];
  
  [self setupPairsToGo];
  [self setupScore];
  [self setupGameboard];
}

- (IBAction) restartButtonTapped: (id) sender
{
  /* Setup the game all over again */
  
  [self invalidateGameboard];
  
  [self setupPairsToGo];
  [self setupScore];
  [self setupGameboard];
}

- (IBAction) cardButtonTapped: (id) sender
{
  if (!self.isOnDelay)
  {
    UIButton* button = (UIButton*) sender;
    UIButton* selectedButton = self.currentlySelectedButton;
    if (button != selectedButton)
    {
      [button setSelected: YES];
      
      if (selectedButton != nil)
      {
        if (selectedButton.tag == button.tag)
        {
          [button setHidden: YES];
          [selectedButton setHidden: YES];
          
          self.score += 1;
          self.pairsToGo -= 1;
          
          self.currentlySelectedButton = nil;
          
          /* The game is finished */
          if (self.pairsToGo == 0)
          {
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
              UIAlertView* alertView
                = [[UIAlertView alloc] initWithTitle: @"Congrats!"
                                             message: @"Enter your name "
                                                      @"and email"
                                            delegate: self
                                   cancelButtonTitle: @"Play Again"
                                   otherButtonTitles: @"View Highscores", nil];
            
              [alertView setAlertViewStyle:
               UIAlertViewStyleLoginAndPasswordInput];

              [[alertView textFieldAtIndex:1] setSecureTextEntry: NO];
              [[alertView textFieldAtIndex:0] setPlaceholder: @"Name"];
              [[alertView textFieldAtIndex:1] setPlaceholder: @"Email"];
              [alertView show];
            });
          }
        }
        else
        {
          self.isOnDelay = YES;
          
          [self performSelector: @selector(closeCards:)
                     withObject: button
                     afterDelay: 1.0f];
        }
      }
      else
      {
        self.currentlySelectedButton = button;
      }
    }
  }
}

- (void) closeCards: (UIButton*) button
{
  self.isOnDelay = NO;
  
  /* Add delay so that the player can see the selected card */
  [button setSelected: NO];
  [self.currentlySelectedButton setSelected: NO];
  
  /* Subtract the score */
  NSInteger newScore = self.score - 1;
  self.score = MAX(0, newScore);
  
  self.currentlySelectedButton = nil;
}

#pragma mark - Helper methods

- (void) assignButtonsValue: (NSInteger) value
{
  /* Do the assignment twice since the colors will come in pair */
  for (NSInteger i = 0; i < 2; i++)
  {
    NSInteger row = arc4random_uniform(4);
    NSInteger column = arc4random_uniform(4);
    
    BOOL taken = [self.gameBoard isSpaceTakenOnColumn: column
                                               andRow: row];
    
    GameboardPoint* point = [[GameboardPoint alloc] init];
    point.row = row;
    point.column = column;
    
    if (taken)
    {
      point = [self.gameBoard insertValueOnTheFirstEmptySpace: value];
    }
    else
    {
      [self.gameBoard insertValue: (NSInteger) value
                         onColumn: (NSInteger) column
                           andRow: (NSInteger) row];
    }
    
    /* Find the button that corresponds to the point in our gameboard */
    
    NSString* imageTitle = [NSString stringWithFormat: @"colour%ld",
                            (long)value];
    UIImage* backgroundImage = [UIImage imageNamed: imageTitle];
    UIButton* button = nil;
    
    switch (point.row)
    {
      case 0:
      {
        button = [self.firstRowButtons objectAtIndex: point.column];
        break;
      }
        
      case 1:
      {
        button = [self.secondRowButtons objectAtIndex: point.column];
        break;
      }
        
      case 2:
      {
        button = [self.thirdRowButtons objectAtIndex: point.column];
        break;
      }
        
      case 3:
      {
        button = [self.fourthRowButtons objectAtIndex: point.column];
        break;
      }
        
      default:
      {
        break;
      }
    }
    button.tag = value;
    
    /* Set selected state */
    [button setBackgroundImage: backgroundImage
                      forState: UIControlStateSelected];
  }
}

#pragma mark - Navigation methods

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id)                 sender
{
  /* Pass the current score */
  if ([segue.destinationViewController isKindOfClass:
       [HighScoreViewController class]])
  {
    HighScoreViewController* controller
      = (HighScoreViewController*) segue.destinationViewController;
    controller.playerScore = self.playerScore;
  }
}

#pragma mark - UIAlertViewDelegate methods

- (void)        alertView: (UIAlertView*) alertView
didDismissWithButtonIndex: (NSInteger)    buttonIndex
{
  if (buttonIndex == 1)
  {
    HighScore* score = [[HighScore alloc] init];
    score.name = [alertView textFieldAtIndex: 0].text;
    score.email = [alertView textFieldAtIndex: 1].text;
    score.score = [NSNumber numberWithInteger: self.score];
    
    self.playerScore = score;
    
    [self performSegueWithIdentifier: kToHighScoreScreenSegueID
                              sender: nil];
  }
  else
  {
    /* Setup the game all over again */
    
    [self invalidateGameboard];
    
    [self setupPairsToGo];
    [self setupScore];
    [self setupGameboard];
  }
}

@end
