//
//  GameboardPoint.h
//  ColorMemory
//
//  Created by Raymond Brion on 19/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameboardPoint : NSObject

@property (nonatomic, assign) NSInteger row;
@property (nonatomic, assign) NSInteger column;

@end
