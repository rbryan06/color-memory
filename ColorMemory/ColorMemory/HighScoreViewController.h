//
//  HighScoreViewController.h
//  ColorMemory
//
//  Created by Raymond Brion on 20/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HighScore.h"

@interface HighScoreViewController : UITableViewController

@property (nonatomic, strong) HighScore* playerScore;

@end
