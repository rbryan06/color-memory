//
//  HighScoreJSONParser.h
//  ColorMemory
//
//  Created by Raymond Brion on 20/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HighScore.h"

@interface HighScoreJSONParser : NSObject

- (NSArray*) retreiveHighScores;

- (void) putHighScore: (HighScore*) highScore;

@end
