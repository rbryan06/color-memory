//
//  Custom2DArray.m
//  ColorMemory
//
//  Created by Raymond Brion on 19/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "Custom2DArray.h"

@interface Custom2DArray ()

@property (nonatomic, strong) NSMutableArray* board;

@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;

@end

@implementation Custom2DArray

#pragma mark - Initialization

- (instancetype) initWithHeight: (NSInteger) height
                       andWidth: (NSInteger) width
{
  self = [super init];
  
  if (self)
  {
    self.height = height;
    self.width = width;
    [self setupBoard];
  }
  return self;
}

#pragma mark - Setup methods

- (void) setupBoard
{
  self.board = [[NSMutableArray alloc] initWithCapacity: self.height];
  
  /* Pre-populate the board with 0s */
  for (NSInteger i = 0; i < self.height; i++)
  {
    NSMutableArray* subArray = [[NSMutableArray alloc]
                                initWithCapacity: self.width];
    for (NSInteger j = 0; j < self.width; j++)
    {
      NSNumber* value = [NSNumber numberWithInt: 0];
      [subArray insertObject: value
                     atIndex: j];
    }
    [self.board insertObject: subArray
                     atIndex: i];
  }
}

#pragma mark - Public methods

- (BOOL) isSpaceTakenOnColumn: (NSInteger) column
                       andRow: (NSInteger) row
{
  /* A value of 0 will indicate that the space is blank */
  NSInteger value = [self valueForSpaceOnColumn: column
                                         andRow: row];
  return value != 0;
}

- (NSInteger) valueForSpaceOnColumn: (NSInteger) column
                             andRow: (NSInteger) row
{
  NSArray* subArray = [self.board objectAtIndex: row];
  NSNumber* value = [subArray objectAtIndex: column];
  return value.integerValue;
}

- (void) insertValue: (NSInteger) value
            onColumn: (NSInteger) column
              andRow: (NSInteger) row
{
  NSMutableArray* subArray = [self.board objectAtIndex: row];
  [subArray replaceObjectAtIndex: column
                      withObject: [NSNumber numberWithInteger: value]];
}

- (GameboardPoint*) insertValueOnTheFirstEmptySpace: (NSInteger) value
{
  /* Find the first empty space */
  for (NSInteger i = 0; i < self.height; i++)
  {
    NSMutableArray* subArray = [self.board objectAtIndex: i];
    for (NSInteger j = 0; j < self.width; j++)
    {
      NSNumber* currentValue = [subArray objectAtIndex: j];
      
      /* We already found the first empty space */
      if (currentValue.integerValue == 0)
      {
        [subArray replaceObjectAtIndex: j
                            withObject: [NSNumber numberWithInteger: value]];
        
        /* Return the spot where we inserted the value */
        GameboardPoint* point = [[GameboardPoint alloc] init];
        point.row = i;
        point.column = j;
        return point;
      }
    }
  }
  return nil;
}

- (NSString*) description
{
  NSMutableString* string = [[NSMutableString alloc] init];
  
  for (NSInteger i = 0; i < self.height; i++)
  {
    NSMutableArray* subArray = [self.board objectAtIndex: i];
    [string appendFormat: @"%@", subArray];
    [string appendString: @"\n"];
  }
  return string;
}

@end
