//
//  HighScore.h
//  ColorMemory
//
//  Created by Raymond Brion on 20/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HighScore : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSNumber* score;

@end
