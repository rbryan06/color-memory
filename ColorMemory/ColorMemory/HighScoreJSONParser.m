//
//  HighScoreJSONParser.m
//  ColorMemory
//
//  Created by Raymond Brion on 20/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "HighScoreJSONParser.h"

@implementation HighScoreJSONParser

- (NSArray*) retreiveHighScores
{
  /* Determine if the scores.json is existing */
  
  NSMutableArray* scores = [[NSMutableArray alloc] init];
  
  NSFileManager* fileManager = [NSFileManager defaultManager];
  
  NSArray* paths
    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
  NSString* documentsDirectory = [paths objectAtIndex: 0];

  NSString* appFile
    = [documentsDirectory stringByAppendingPathComponent: @"scores.json"];
  
  if ([fileManager fileExistsAtPath: appFile])
  {
    /* Read the file */
    
    NSURL* scoreURL = [NSURL fileURLWithPath: appFile];
    
    NSData* scoreData = [NSData dataWithContentsOfURL: scoreURL];
    
    NSDictionary* scoreDictionary
      = [NSJSONSerialization JSONObjectWithData: scoreData
                                        options: kNilOptions
                                          error: nil];
    
    /* Iterate to all the stored scores */
    
    NSArray* scoreArray = [scoreDictionary objectForKey: @"scores"];
    
    for (NSDictionary* score in scoreArray)
    {
      HighScore* highScore = [[HighScore alloc] init];
      highScore.name = [score objectForKey: @"name"];
      highScore.email = [score objectForKey: @"email"];
      highScore.score = [score objectForKey: @"score"];
      
      [scores addObject: highScore];
    }
  }
  return scores;
}

- (void) putHighScore: (HighScore*) highScore
{
  NSArray* paths
    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                          NSUserDomainMask,
                                          YES);
  NSString* documentsDirectory = [paths objectAtIndex: 0];
  NSString* appFile
    = [documentsDirectory stringByAppendingPathComponent: @"scores.json"];
  NSURL* scoreURL = [NSURL fileURLWithPath: appFile];
  
  NSMutableDictionary* scoreDictionary = nil;
  NSMutableArray* scoreArray = nil;
  
  /* Create the file if it is not existing */
  
  if ([[NSFileManager defaultManager] fileExistsAtPath: scoreURL.path])
  {
    /* Read the contents of the file if it is existing */
    
    NSData* scoreData = [NSData dataWithContentsOfURL: scoreURL];
    
    scoreDictionary
      = ((NSDictionary*)[NSJSONSerialization JSONObjectWithData: scoreData
                                                        options: kNilOptions
                                                          error: nil])
        .mutableCopy;
    
    scoreArray
      = ((NSArray*)[scoreDictionary objectForKey: @"scores"]).mutableCopy;
  }
  else
  {
    scoreDictionary = [[NSMutableDictionary alloc] init];
    scoreArray = [[NSMutableArray alloc] init];
    
    [scoreDictionary setObject: scoreArray
                        forKey: @"scores"];
  }
  
  /* Populate the high score entry */
  
  NSMutableDictionary* highScoreDictionary
    = [[NSMutableDictionary alloc] initWithCapacity: 3];
  [highScoreDictionary setObject: highScore.name
                          forKey: @"name"];
  [highScoreDictionary setObject: highScore.email
                          forKey: @"email"];
  [highScoreDictionary setObject: highScore.score
                          forKey: @"score"];
  
  [scoreArray addObject: highScoreDictionary];

  [scoreDictionary setObject: scoreArray
                      forKey: @"scores"];
  
  NSError* writeError = nil;
  
  NSData* newScoresData
    = [NSJSONSerialization dataWithJSONObject: scoreDictionary
                                      options: NSJSONWritingPrettyPrinted
                                        error: &writeError];
  
  if (writeError == nil)
  {
    [newScoresData writeToFile: scoreURL.path
                    atomically: YES];
  }
}

@end
