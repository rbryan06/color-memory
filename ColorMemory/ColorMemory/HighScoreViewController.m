//
//  HighScoreViewController.m
//  ColorMemory
//
//  Created by Raymond Brion on 20/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "HighScoreViewController.h"
#import "HighScoreJSONParser.h"

@interface HighScoreViewController ()

@property (nonatomic, strong) NSArray* scores;

@property (nonatomic, assign) NSInteger playerScoreIndex;

@end

@implementation HighScoreViewController

#pragma mark - View lifecycle

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  /* Retrieve all existing high scores */
  HighScoreJSONParser* parser = [[HighScoreJSONParser alloc] init];
  NSMutableArray* tempScores = [parser retreiveHighScores].mutableCopy;
  
  [tempScores addObject: self.playerScore];

  [parser putHighScore: self.playerScore];
  
  /* Sort the scores */
  NSSortDescriptor* sortDescriptor
    = [[NSSortDescriptor alloc] initWithKey: @"score"
                                  ascending: NO];
  NSArray* descriptors = [NSArray arrayWithObject: sortDescriptor];
  NSArray* sortedArray = [tempScores sortedArrayUsingDescriptors: descriptors];
  self.scores = sortedArray;
  
  self.playerScoreIndex = [self.scores indexOfObject: self.playerScore];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
  NSInteger defaultRows = self.scores.count;
  
  /* Player's score is below the top 10 */
  if (defaultRows >= 10 && self.playerScoreIndex > 9)
  {
    defaultRows = 11;
  }
  return defaultRows;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
  NSString* reuseIdentifier = @"scores";
  
  UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:
                           reuseIdentifier];
  
  HighScore* score = nil;
  NSInteger rank = indexPath.row + 1;
  
  if (self.playerScoreIndex > 9)
  {
    rank = self.playerScoreIndex + 1;
    score = [self.scores objectAtIndex: self.playerScoreIndex];
  }
  else
  {
    score = [self.scores objectAtIndex: indexPath.row];
  }
  
  if (cell == nil)
  {
    cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1
                                  reuseIdentifier: reuseIdentifier];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  }
  
  cell.textLabel.text = [NSString stringWithFormat: @"#%ld %@ - %@",
                         rank,
                         score.name,
                         score.email];
  cell.detailTextLabel.text = [NSString stringWithFormat: @"Score: %ld",
                               score.score.integerValue];
  
  cell.accessoryType = UITableViewCellAccessoryNone;
  
  return cell;
}

@end
