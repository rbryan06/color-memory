//
//  Custom2DArray.h
//  ColorMemory
//
//  Created by Raymond Brion on 19/03/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameboardPoint.h"

@interface Custom2DArray : NSObject

- (instancetype) initWithHeight: (NSInteger) height
                       andWidth: (NSInteger) width;

- (BOOL) isSpaceTakenOnColumn: (NSInteger) column
                       andRow: (NSInteger) row;

- (NSInteger) valueForSpaceOnColumn: (NSInteger) column
                             andRow: (NSInteger) row;

- (void) insertValue: (NSInteger) value
            onColumn: (NSInteger) column
              andRow: (NSInteger) row;

- (GameboardPoint*) insertValueOnTheFirstEmptySpace: (NSInteger) value;

@end
