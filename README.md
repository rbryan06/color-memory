# ColorMemory
Color Memory Game iOS Exercise

TODO:

Improvements needed:

Design for the whole app
Game board (Number of pairs) option to increase or decrease
Timer. Include a time variable for the high score that will serve as a tie-breaker for tie scores. Include the timer in the game info view together with the score
Welcome screen that will allow users to proceed to the game or show the high scores and even a screen for instructions
Better JSON parser/mapper that can translate a POJO to json file and vice versa.
Animation when opening a card, flip animation. Make use of UIAnimationOptionTransitionFlipFromLeft.
Make use of a cloud service like Parse or an own  API server to store the score of the user to have a global leaderboard.
